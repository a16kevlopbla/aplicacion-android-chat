package com.example.ausias.pt22_chat;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    ArrayList<ChatMessage> messageList;
    String username1;
    String username2;

    public ChatAdapter(String username1, String username2){
        this.username1 = username1;
        this.username2 = username2;
        messageList = new ArrayList<ChatMessage>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch(viewType){
            case 1:
                View viewLeftMessage = inflater.inflate(R.layout.leftmessage,parent,false);
                ViewHolder leftMessageViewHolder = new ViewHolder(viewLeftMessage);
                return leftMessageViewHolder;
            case 2:
                View viewRightMessage = inflater.inflate(R.layout.rigthmessage,parent,false);
                ViewHolder rightMessageViewHolder = new ViewHolder(viewRightMessage);
                return rightMessageViewHolder;
            case 11:
                View viewLeftImage = inflater.inflate(R.layout.leftimage,parent,false);
                ViewHolder leftImageViewHolder = new ViewHolder(viewLeftImage);
                return leftImageViewHolder;
            case 22:
                View viewRightImage = inflater.inflate(R.layout.rigthimage,parent,false);
                ViewHolder rightImageViewHolder = new ViewHolder(viewRightImage);
                return rightImageViewHolder;
            case 111:
                View viewLeftBigImage = inflater.inflate(R.layout.leftimage,parent,false);
                ViewHolder leftBigImageViewHolder = new ViewHolder(viewLeftBigImage);
                return leftBigImageViewHolder;
            case 222:
                View viewRightBigImage = inflater.inflate(R.layout.rigthimage,parent,false);
                ViewHolder rightBigImageViewHolder = new ViewHolder(viewRightBigImage);
                return rightBigImageViewHolder;
            default:
                break;
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        ChatMessage message = messageList.get(position);
        if(message.getUsernameSend().equals(username1)) {
            if(message.getThumbnail()!=null){
                return 11;
            }else if(message.getBigImagePath()!=null){
                return 111;
            }
            return 1;
        }
        if(message.getUsernameSend().equals(username2)){
            if(message.getThumbnail()!=null){
                return 22;
            }else if(message.getBigImagePath()!=null) {
                return 222;
            }
            return 2;
        }
        return 0;
    }

    @Override
    public void onBindViewHolder(ChatAdapter.ViewHolder holder, int position) {
        ChatMessage message = messageList.get(position);
        switch(getItemViewType(position)){
            case 1:
                holder.leftTextView.setText(message.message);
                holder.leftTextView.setBackgroundResource(R.drawable.bubblechat11);
                break;
            case 2:
                holder.rightTextView.setText(message.message);
                holder.rightTextView.setBackgroundResource(R.drawable.bubblechat22);
                break;
            case 11:
                holder.leftImageView.setImageBitmap(message.getThumbnail());
                break;
            case 22:
                holder.rightImageView.setImageBitmap(message.getThumbnail());
                break;
            case 111:
                holder.leftImageView.setImageBitmap(message.getBigImagePath());
                break;
            case 222:
                holder.rightImageView.setImageBitmap(message.getBigImagePath());
                break;
            default:
                break;
        }
    }



    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView leftTextView;
        public TextView rightTextView;
        public ImageView leftImageView;
        public ImageView rightImageView;

        public ViewHolder(View itemView) {
            super(itemView);

            leftTextView = itemView.findViewById(R.id.leftTextView);
            rightTextView = itemView.findViewById(R.id.rightTextView);

            leftImageView = itemView.findViewById(R.id.leftImageView);
            rightImageView = itemView.findViewById(R.id.rightImageView);
        }
    }

    public String getUsername1() {
        return username1;
    }

    public void setUsername1(String username1) {
        this.username1 = username1;
    }

    public String getUsername2() {
        return username2;
    }

    public void setUsername2(String username2) {
        this.username2 = username2;
    }

    public ArrayList<ChatMessage> getMessageList() {
        return messageList;
    }

    public void setMessageList(ArrayList<ChatMessage> messageList) {
        this.messageList = messageList;
    }

    public void addMessageToList(ChatMessage message){
        messageList.add(message);
        notifyDataSetChanged();
    }
}
