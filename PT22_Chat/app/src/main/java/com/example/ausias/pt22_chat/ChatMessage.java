package com.example.ausias.pt22_chat;

import android.graphics.Bitmap;

public class ChatMessage {
    String usernameSend;
    String usernameReceive;
    String message;
    Bitmap bigImagePath;
    Bitmap thumbnail;

    public ChatMessage(String usernameSend, String usernameReceive, String message, Bitmap bigImagePath, Bitmap thumbnail){
        this.usernameSend = usernameSend;
        this.usernameReceive = usernameReceive;
        this.message = message;
        this.bigImagePath = bigImagePath;
        this.thumbnail = thumbnail;
    }

    public Bitmap getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Bitmap thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Bitmap getBigImagePath() {
        return bigImagePath;
    }

    public void setBigImagePath(Bitmap bigImagePath) {
        this.bigImagePath = bigImagePath;
    }

    public String getUsernameSend() {
        return usernameSend;
    }

    public void setUsernameSend(String usernameSend) {
        this.usernameSend = usernameSend;
    }

    public String getUsernameReceive() {
        return usernameReceive;
    }

    public void setUsernameReceive(String usernameReceive) {
        this.usernameReceive = usernameReceive;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
