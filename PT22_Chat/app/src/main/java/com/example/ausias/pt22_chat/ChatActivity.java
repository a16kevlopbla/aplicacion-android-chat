package com.example.ausias.pt22_chat;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;

public class ChatActivity extends AppCompatActivity {

    String username1;
    String username2;
    ChatAdapter chatAdapter;
    static final int REQUEST_IMAGE_CAPTURE_PETITA_SEND1 = 1;
    static final int REQUEST_IMAGE_CAPTURE_PETITA_SEND2 = 2;
    static final int REQUEST_IMAGE_CAPTURE_GRAN_SEND1 = 3;
    static final int REQUEST_IMAGE_CAPTURE_GRAN_SEND2 = 4;
    File picFile;
    int id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        TextView textViewUser1 = findViewById(R.id.textViewUser1);
        TextView textViewUser2 = findViewById(R.id.textViewUser2);
        textViewUser1.setText(getIntent().getStringExtra("Username1"));
        textViewUser2.setText(getIntent().getStringExtra("Username2"));
        username1 = textViewUser1.getText().toString();
        username2 = textViewUser2.getText().toString();

        RecyclerView recyclerView = findViewById(R.id.recyclerId);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        chatAdapter = new ChatAdapter(username1, username2);

        recyclerView.setAdapter(chatAdapter);
    }

    public void sendMessage1(View view) {
        EditText messageText = findViewById(R.id.editTextMessages);
        if (messageText.getText().length() > 0) {
            ChatMessage message = new ChatMessage(username1, username2, messageText.getText().toString(), null, null);
            chatAdapter.addMessageToList(message);
        }
    }

    public void sendMessage2(View view) {
        EditText messageText = findViewById(R.id.editTextMessages);
        if (messageText.getText().length() > 0) {
            ChatMessage message = new ChatMessage(username2, username1, messageText.getText().toString(), null, null);
            chatAdapter.addMessageToList(message);
        }
    }

    public void sendImage1(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE_PETITA_SEND1);
        }
    }

    public void sendImage2(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE_PETITA_SEND2);
        }
    }

    //File privateExternalDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    //File publicExternalDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

    /*Uri to String
     Uri uri;
     String stringUri;
     stringUri = uri.toString();

     String to Uri
     Uri uri;
     String stringUri;
     uri = Uri.parse(stringUri);*/

    public void sendBigImage1(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File privateDirectory = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        String nomFitxer = "foto" + id;
        id++;

        try {
            picFile = File.createTempFile(
                    nomFitxer,            /* prefix */
                    ".png",         /* suffix */
                    privateDirectory      /* directory */
            );

            if (intent.resolveActivity(getPackageManager()) != null) {
                Uri photoURI = FileProvider.getUriForFile(this, "com.example.ausias.pt22_chat.fileprovider", picFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE_GRAN_SEND1);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void sendBigImage2(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File privateDirectory = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        String nomFitxer = "foto" + id;
        id++;

        try {
            picFile = File.createTempFile(
                    nomFitxer,            /* prefix */
                    ".png",         /* suffix */
                    privateDirectory      /* directory */
            );

            if (intent.resolveActivity(getPackageManager()) != null) {
                Uri photoURI = FileProvider.getUriForFile(this, "com.example.ausias.pt22_chat.fileprovider", picFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE_GRAN_SEND2);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
    private void setPicWidthGlide(ImageView mImageView) {
        //ImageView mImageView = (ImageView) findViewById(R.id.imageView);
        Glide.with (this).load(picfile).into(mImageView);
    }
    */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_IMAGE_CAPTURE_PETITA_SEND1:
                    Bundle extras = data.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");

                    ChatMessage image = new ChatMessage(username1, username2, null, null, imageBitmap);
                    chatAdapter.addMessageToList(image);
                    break;
                case REQUEST_IMAGE_CAPTURE_PETITA_SEND2:
                    Bundle extras2 = data.getExtras();
                    Bitmap imageBitmap2 = (Bitmap) extras2.get("data");

                    ChatMessage image2 = new ChatMessage(username2, username1, null, null, imageBitmap2);
                    chatAdapter.addMessageToList(image2);
                    break;
                case REQUEST_IMAGE_CAPTURE_GRAN_SEND1:
                    ImageView imageView1 = (ImageView) findViewById(R.id.leftImageView);
                    setPic(imageView1,1 );
                    break;
                case REQUEST_IMAGE_CAPTURE_GRAN_SEND2:
                    ImageView imageView2 = (ImageView) findViewById(R.id.rightImageView);
                    setPic(imageView2,2 );
                    break;
                default:
                    break;
            }
        }

    }

    private void setPic(ImageView imageView, int position) {
        int targetW = 200;
        int targetH = 200;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(picFile.getAbsolutePath(), bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(picFile.getAbsolutePath(), bmOptions);
        if(position==1){
            ChatMessage image = new ChatMessage(username1, username2, null, bitmap, null);
            chatAdapter.addMessageToList(image);
        }else{
            ChatMessage image = new ChatMessage(username2, username1, null, bitmap, null);
            chatAdapter.addMessageToList(image);
        }
    }
}