package com.example.ausias.pt22_chat;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ChatDatabase extends SQLiteOpenHelper {

    Context context;

    public ChatDatabase (Context context){
        super(context,Dades.NOM_DB,null,1);
        this.context=context;
    }

    public static class Dades{
        public static final String NOM_DB = "chatdb";
        public static final String NOM_TAULA = "chats";
        public static final String COLUMNA_USUARI1 = "usuari1";
        public static final String COLUMNA_USUARI2 = "usuari2";
        public static final String COLUMNA_MESSAGE = "message";
        public static final String COLUMNA_IMAGEPATH = "imagepath";
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS "+Dades.NOM_TAULA+" ("+Dades.COLUMNA_USUARI1+" TEXT, "+Dades.COLUMNA_USUARI2+" TEXT, "+Dades.COLUMNA_MESSAGE+" TEXT, "+Dades.COLUMNA_IMAGEPATH+" TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //Nothing to do
    }


}
