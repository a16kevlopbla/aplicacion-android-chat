package com.example.ausias.pt22_chat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openChat(View view){
        Intent chatIntent = new Intent(this,ChatActivity.class);
        EditText user1 = findViewById(R.id.username1);
        EditText user2 = findViewById(R.id.username2);
        chatIntent.putExtra("Username1",user1.getText().toString());
        chatIntent.putExtra("Username2",user2.getText().toString());
        startActivity(chatIntent);
    }
}
